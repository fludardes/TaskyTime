extern crate chrono;
extern crate rand;
extern crate tasky_time;

use tasky_time::app;
use tasky_time::utils;
use tasky_time::utils::test;

#[test]
fn main_test() {
  // Test new task creation (inside temporary root)
  test::run_temp_root(|root| {
    let temp_str = root.to_str().unwrap();
    let date = utils::today_name();

    let args = &["tasky-time", "task", "-a", "Task #1"];
    app::run(Some(args), Some(temp_str)).expect("App run failed");

    let mut day_file = root.clone();
    day_file.push("data");
    day_file.push(format!("{}.json", date));

    let contents = test::read_to_string(day_file);

    assert_eq!(contents, r#"{"tasks":[{"name":"Task #1","time":0}]}"#);
  });
}
