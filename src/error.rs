use std::error;
use std::fmt;
use std::io;

#[derive(Debug)]
pub struct Error {
  message: String,
}

impl Error {
  pub fn new<S: Into<String>>(message: S) -> Error {
    Error {
      message: message.into(),
    }
  }

  pub fn with_reason<S: Into<String>>(message: S, reason: &error::Error) -> Error {
    let message = format!("{}, caused by: {}", message.into(), reason.to_string());

    Error { message }
  }

  pub fn message(&self) -> &str {
    &self.message
  }
}

impl error::Error for Error {
  fn cause(&self) -> Option<&error::Error> {
    None
  }
}

impl fmt::Display for Error {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", self.message())
  }
}

impl From<io::Error> for Error {
  fn from(err: io::Error) -> Error {
    Error::with_reason("IO Error", &err)
  }
}
