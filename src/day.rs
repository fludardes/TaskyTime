use std::cell::{Ref, RefCell, RefMut};
use std::rc::Rc;

use task::{Task, TaskItem};

use utils;

pub struct Day {
  name: RefCell<String>,
  tasks: RefCell<Vec<TaskItem>>,
}

impl Day {
  pub fn new<S: Into<String>>(name: S, tasks: Vec<TaskItem>) -> Day {
    Day {
      name: RefCell::new(name.into()),
      tasks: RefCell::new(tasks),
    }
  }

  pub fn add_task(&self, task: Task) -> TaskItem {
    self.tasks.borrow_mut().push(Rc::new(task));
    // Should not fail
    let tasks = self.tasks();
    let task = tasks.last().expect("Added task expected");
    Rc::clone(task)
  }

  pub fn tasks(&self) -> Ref<Vec<TaskItem>> {
    self.tasks.borrow()
  }

  pub fn set_tasks(&self, tasks: Vec<TaskItem>) {
    self.tasks.replace(tasks);
  }

  pub fn tasks_mut(&self) -> RefMut<Vec<TaskItem>> {
    self.tasks.borrow_mut()
  }

  pub fn show(&self) {
    println!("DAY {} TASKS:", self.name());

    for (i, task) in self.tasks().iter().enumerate() {
      println!(
        " {} | {} | {}",
        i + 1,
        utils::format_time(&task.time()),
        task.name()
      );
    }
  }

  pub fn name(&self) -> Ref<String> {
    self.name.borrow()
  }

  pub fn set_name<S: Into<String>>(&self, name: S) {
    self.name.replace(name.into());
  }

  // NOTE: Indexed from 1 to not be confused with show
  pub fn remove_task(&self, id: usize) {
    self.tasks.borrow_mut().remove(id - 1);
  }

  pub fn task_by_number(&self, num: usize) -> Option<Rc<Task>> {
    self.tasks_mut().get(num - 1).map(|item| Rc::clone(&item))
  }

  pub fn task_id_to_number(&self, id: &str) -> Option<usize> {
    match id.parse::<usize>() {
      Ok(num) => Some(num),
      Err(_) => self.task_number_by_name(&id),
    }
  }

  pub fn task_by_id(&self, id: &str) -> Option<Rc<Task>> {
    let number = match self.task_id_to_number(id) {
      Some(num) => num,
      None => return None,
    };
    self.task_by_number(number)
  }

  pub fn task_number_by_name(&self, name: &str) -> Option<usize> {
    // TODO: Use iterator `find` method?
    for (i, task) in self.tasks().iter().enumerate() {
      if task.name().as_str() == name {
        return Some(i + 1);
      }
    }
    None
  }

  // Replace day data with new day
  pub fn replace(&self, day: Day) {
    self.set_name(day.name().clone());
    self.set_tasks(day.tasks().clone());
  }
}
