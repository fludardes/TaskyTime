use super::TaskSave;
use day::Day;

#[derive(Serialize, Deserialize, Debug)]
pub struct DaySave {
  tasks: Vec<TaskSave>,
}

impl DaySave {
  pub fn tasks(&self) -> &[TaskSave] {
    self.tasks.as_slice()
  }
}

impl<'a> From<&'a Day> for DaySave {
  fn from(day: &Day) -> DaySave {
    let tasks = day
      .tasks()
      .iter()
      .map(|task| TaskSave::from(task.clone()))
      .collect::<Vec<TaskSave>>();

    DaySave { tasks }
  }
}
