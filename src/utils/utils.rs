use chrono::prelude::*;
use log;
use log::LevelFilter;
use logme;
use regex::{Captures, Regex};
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::process;
use std::time::Duration;

use error::Error;

lazy_static! {
  static ref TIME_LONG_REGEX: Regex =
    Regex::new(r"^-?(\d{2}):(\d{2}):(\d{2})$").expect("Failed to build long time regex");
  static ref TIME_SHORT_REGEX: Regex =
    Regex::new(r"^-?(\d{2}):(\d{2})$").expect("Failed to build short time regex");
}

#[allow(dead_code)]
pub fn read_to_string<P: AsRef<Path>>(file: P) -> Result<String, Error> {
  let mut file = File::open(file.as_ref())?;
  let mut contents = String::new();
  file
    .read_to_string(&mut contents)
    .map_err(|err| Error::with_reason("Failed to read file to string", &err))?;
  Ok(contents)
}

pub fn today_name() -> String {
  let date: DateTime<Local> = Local::now();
  date.format("%Y-%m-%d").to_string()
}

#[allow(dead_code)]
pub fn config_dir() -> PathBuf {
  let home = if let Ok(var) = env::var("HOME") {
    PathBuf::from(var)
  } else if let Ok(var) = env::var("AppData") {
    PathBuf::from(var)
  } else {
    env::temp_dir()
  };

  let mut path = PathBuf::from(home);
  path.push(".config");
  path
}

fn zero_fmt(n: u64) -> String {
  if n >= 10 {
    format!("{}", n)
  } else {
    format!("0{}", n)
  }
}

pub fn format_time(time: &Duration) -> String {
  let seconds = time.as_secs();
  let hours = seconds
    .checked_div(3600)
    .expect("Show time division failed");
  let minutes = (seconds - (hours * 3600))
    .checked_div(60)
    .expect("Show time minutes division failed");
  let seconds = seconds - (hours * 3600) - (minutes * 60);
  format!(
    "{}:{}:{}",
    zero_fmt(hours),
    zero_fmt(minutes),
    zero_fmt(seconds)
  )
}


pub fn format_time_pretty(time: &Duration) -> String {
  let seconds = time.as_secs();
  let hours = seconds
    .checked_div(3600)
    .expect("Show time division failed");
  let minutes = (seconds - (hours * 3600))
    .checked_div(60)
    .expect("Show time minutes division failed");
  let seconds = seconds - (hours * 3600) - (minutes * 60);
  if hours > 0 {
    return format!(
      "{}:{}:{}",
      zero_fmt(hours),
      zero_fmt(minutes),
      zero_fmt(seconds)
    );
  }
  format!(
    "{}:{}",
    zero_fmt(minutes),
    zero_fmt(seconds)
  )
}

pub fn parse_time<S: AsRef<str>>(time: S) -> Result<Duration, Error> {
  let time = time.as_ref();

  let seconds = if TIME_LONG_REGEX.is_match(time) {
    parse_hhmmss_seconds(&TIME_LONG_REGEX, time)?
  } else if TIME_SHORT_REGEX.is_match(time) {
    parse_mmss_seconds(&TIME_SHORT_REGEX, time)?
  } else {
    return Err(Error::new(format!("Unknown time format: {}", time)));
  };

  Ok(Duration::from_secs(seconds))
}

fn parse_mmss_seconds(regex: &Regex, time: &str) -> Result<u64, Error> {
  let caps = regex
    .captures(time)
    .ok_or(Error::new(format!("Failed to parse time {}", time)))?;

  let mm = capture_u64(&caps, 1)?;
  let ss = capture_u64(&caps, 2)?;

  if ss > 59 {
    return Err(Error::new(format!(
      "Max allowed seconds is {}, received {}",
      59, ss
    )));
  };

  let seconds = (mm * 60) + ss;

  Ok(seconds)
}

fn capture_u64(captures: &Captures, index: usize) -> Result<u64, Error> {
  let res = captures
    .get(index)
    .ok_or(Error::new(format!(
      "Failed to get captures index {}",
      index
    )))?.as_str();

  Ok(
    res
      .parse::<u64>()
      .map_err(|err| Error::with_reason(format!("Failed to parse {} as u64", res), &err))?,
  )
}

fn parse_hhmmss_seconds(regex: &Regex, time: &str) -> Result<u64, Error> {
  let caps = regex
    .captures(time)
    .ok_or(Error::new(format!("Failed to parse time {}", time)))?;

  let hh = capture_u64(&caps, 1)?;
  let mm = capture_u64(&caps, 2)?;
  let ss = capture_u64(&caps, 3)?;

  if mm > 59 {
    return Err(Error::new(format!(
      "Max allowed minutes is {}, received {}",
      59, mm
    )));
  };

  if ss > 59 {
    return Err(Error::new(format!(
      "Max allowed seconds is {}, received {}",
      59, ss
    )));
  };

  let seconds = (hh * 60 * 60) + (mm * 60) + ss;

  Ok(seconds)
}

pub fn resources_path() -> PathBuf {
  let executable = env::current_exe();

  let mut path = if let Ok(executable) = executable {
    if let Some(parent) = executable.parent() {
      parent.to_owned()
    } else {
      PathBuf::new()
    }
  } else {
    PathBuf::new()
  };

  path.push("resources");

  if !path.exists() {
    debug!(
      "Guessed resources path {} not exists...",
      path.to_str().expect("Failed to parse resources path")
    );
    path = PathBuf::from("resources");
  }

  path
}

pub fn log_init(verbose: u64) -> Result<(), Error> {
  logme::init()
    .map_err(|err| Error::new(format!("Failed to initialize logger: {}", err.to_string())))?;

  let level = match verbose {
    0 => LevelFilter::Error,
    1 => LevelFilter::Info,
    2 => LevelFilter::Debug,
    3 => LevelFilter::Trace,
    _ => {
      println!("Oops, Cthulhu was waked up, I'm leaving...");
      process::exit(0);
    }
  };

  log::set_max_level(level);

  Ok(())
}

#[cfg(test)]
mod tests {
  use super::{capture_u64, parse_time};
  use regex::Regex;

  #[test]
  fn capture_u64_test() {
    let s = "[42]";
    let caps = Regex::new(r"^\[(\d\d)\]$").unwrap().captures(s).unwrap();

    let n = capture_u64(&caps, 1).unwrap();
    assert_eq!(n, 42);
  }

  #[test]
  fn parse_time_test() {
    assert_eq!(parse_time("01:13:51").unwrap().as_secs(), 4431);
    assert_eq!(parse_time("00:09:09").unwrap().as_secs(), 549);
    assert_eq!(parse_time("09:09").unwrap().as_secs(), 549);
    assert_eq!(parse_time("59:59").unwrap().as_secs(), 3599);
  }
}
