/* Utils for tests */
extern crate rand;

use std::env;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::thread;

pub fn read_to_string<P: AsRef<Path>>(file: P) -> String {
  let mut file = File::open(file.as_ref()).unwrap();
  let mut contents = String::new();
  file.read_to_string(&mut contents).unwrap();
  contents
}

pub fn create_temp_root() -> PathBuf {
  let uniq = rand::random::<u16>();
  let mut tmp = env::temp_dir();
  tmp.push(format!("tasky-time-{}", uniq));

  fs::create_dir(&tmp).unwrap();

  tmp
}

pub fn remove_temp_root<P: AsRef<Path>>(root: P) {
  fs::remove_dir_all(root.as_ref()).unwrap();
}

pub fn run_temp_root<F>(f: F)
where
  F: FnOnce(PathBuf) -> () + Send + 'static,
{
  let tmp = create_temp_root();

  let child = {
    let tmp = tmp.clone();

    thread::spawn(move || f(tmp))
  };

  let res = child.join();

  match res {
    Ok(_) => {
      remove_temp_root(tmp);
    }
    Err(err) => {
      remove_temp_root(tmp);
      panic!("Error: {:?}", err);
    }
  }
}
