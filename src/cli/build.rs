use clap;
use clap::{Arg, ArgMatches, SubCommand};
use conf;

pub fn build<'a>(args: Option<&[&str]>) -> ArgMatches<'a> {
  let about_msg = t!("Track time you spend on tasks");
  let app = clap::App::new("TaskyTime")
    .version(conf::VERSION)
    .author(conf::AUTHORS)
    .about(about_msg.as_str())
    .arg(
      Arg::with_name("verbose")
        .help("Print debug information")
        .short("v")
        .long("verbose")
        .multiple(true),
    ).arg(
      Arg::with_name("day")
        .help("Allows to manually set day")
        .short("d")
        .long("day")
        .takes_value(true),
    ).arg(
      Arg::with_name("cli")
        .help("Disable graphical interface, use only CLI")
        .long("cli"),
    ).subcommand(task_cmd());

  if let Some(args) = args {
    app.get_matches_from(args)
  } else {
    app.get_matches()
  }
}

fn task_cmd<'a, 'b>() -> clap::App<'a, 'b> {
  SubCommand::with_name("task")
    .about("Manage tasks for day")
    // Specify task name
    .arg(Arg::with_name("id")
      .index(1)
      .takes_value(true)
      .help("Task name or number")
      .required_unless_one(&["list"]))
    // Task actions
    .arg(Arg::with_name("add")
      .short("a")
      .long("add")
      .help("Add new task")
      .conflicts_with_all(&["list", "remove", "update", "track"]))
    .arg(Arg::with_name("list")
      .short("l")
      .long("list")
      .help("List tasks in day")
      .conflicts_with_all(&["add", "remove", "update", "track"]))
    .arg(Arg::with_name("remove")
      .short("r")
      .long("remove")
      .help("Remove task")
      .conflicts_with_all(&["add", "list", "update", "track"]))
    .arg(Arg::with_name("update")
      .short("u")
      .long("update")
      .help("Update task info, pass -n (or --name) to change name, and -t (or --track) for time")
      .requires("name")
      .conflicts_with_all(&["add", "list", "remove"]))
    .arg(Arg::with_name("new_name")
      .short("n")
      .long("name")
      .takes_value(true)
      .help("Specify new task name for --update"))
    .arg(Arg::with_name("track")
      .short("t")
      .long("track")
      .takes_value(true)
      .help("Track time in format HH:MM:SS for speificed task or --update entire time count")
      .conflicts_with_all(&["add", "list", "remove"]))
    // Remove unneeded version argument
    .arg(Arg::with_name("version")
      .short("V")
      .long("--version")
      .hidden(true))
}
