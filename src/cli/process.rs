use std::time::Duration;

use super::Cli;
use utils;
use App;
use Error;

impl<'a> Cli<'a> {
  pub fn process(&self, app: &App) -> Result<(), Error> {
    info!("Running in CLI mode");

    let args = if let Some(task_args) = self.args().subcommand_matches("task") {
      task_args
    } else {
      self.show_day(app.day());
      return Ok(());
    };

    if args.is_present("list") {
      self.show_day(app.day());
      return Ok(());
    }

    let id = args
      .value_of("id")
      .ok_or(Error::new("Task id (number or name) missing"))?;

    if args.is_present("add") {
      let name = id;
      app.add_task(name, Duration::from_secs(0))?;
    } else if args.is_present("remove") {
      app.remove_task(id)?;
    } else if args.is_present("update") {
      let time = if let Some(time) = args.value_of("track") {
        Some(utils::parse_time(time)?)
      } else {
        None
      };

      app.update_task(id, args.value_of("new_name"), time)?;
    } else if args.is_present("track") {
      let track = args
        .value_of("track")
        .ok_or(Error::new("Track time required"))?;

      let track = utils::parse_time(track)?;

      app.track_time(id, track)?;
    } else {
      println!("Show task: {}", id);
      return Ok(());
    }

    self.show_day(app.day());

    Ok(())
  }
}
