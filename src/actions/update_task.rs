use std::time::Duration;

use task::TaskItem;
use App;
use Error;

impl App {
  // Add create new task with specified name
  pub fn update_task<I: Into<String>, S: Into<String>>(
    &self,
    id: I,
    name: Option<S>,
    time: Option<Duration>,
  ) -> Result<TaskItem, Error> {
    let id = id.into();

    let day = self.day();
    let task = day
      .task_by_id(id.as_str())
      .ok_or(Error::new(format!("Task with id {} not found", id)))?;

    if let Some(name) = name {
      task.set_name(name.into());
    }

    if let Some(time) = time {
      task.set_time(time);
    }

    self.storage().save(&day)?;

    Ok(task)
  }
}
