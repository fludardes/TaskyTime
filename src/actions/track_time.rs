use std::time::Duration;

use task::TaskItem;
use App;
use Error;

impl App {
  // Track additional time for task
  // `time` — is only delta, not full time!
  pub fn track_time<S: Into<String>>(&self, id: S, time: Duration) -> Result<TaskItem, Error> {
    let id = id.into();
    let day = self.day();
    let task = {
      let err = Error::new(format!(
        "Task with id \"{}\" for track time not found",
        id.as_str()
      ));
      day.task_by_id(id.as_str()).ok_or(err)?
    };
    task.track(time)?;
    self.storage().save(&day)?;
    Ok(task)
  }
}

#[cfg(test)]
mod tests {
  use std::time::Duration;

  use App;
  use Storage;

  use utils;
  use utils::test;

  #[test]
  fn track_time_test() {
    test::run_temp_root(|root| {
      let storage = Storage::init(Some(root)).expect("Storage init failed");
      let app = App::new(storage);
      app
        .init_day(Some("1997-05-26"))
        .expect("Failed to init day");
      app
        .add_task("Test", Duration::from_secs(3600))
        .expect("Failed to add task");
      app
        .track_time("1", Duration::from_secs(2528))
        .expect("Failed to track time");
      let task = app.day().task_by_number(1).expect("Task not found");
      let time = task.time();
      assert_eq!(utils::format_time(&time), "01:42:08");
    });
  }
}
