use App;
use Error;

impl App {
  // Add create new task with specified name
  pub fn remove_task<S: Into<String>>(&self, id: S) -> Result<(), Error> {
    let id = id.into();

    let day = self.day();

    // If text or numeric id
    let task_id = match id.parse::<usize>() {
      Ok(num) => Some(num),
      Err(_) => day.task_number_by_name(&id),
    };

    let task_id = task_id.ok_or(Error::new(format!("Task with id \"{}\" not found", id)))?;

    if day.task_by_number(task_id).is_none() {
      let error = Error::new(format!("Task with id {} not found", task_id));
      return Err(error);
    }

    day.remove_task(task_id);
    self.storage().save(&day)?;

    Ok(())
  }
}
