use std::time::Duration;

use task::TaskItem;
use App;
use Error;
use Task;

impl App {
  // Add create new task with specified name
  pub fn add_task<S: Into<String>>(&self, name: S, time: Duration) -> Result<TaskItem, Error> {
    let name = name.into();
    let task = Task::new(name);
    task.set_time(time);

    let day = self.day();
    let task = day.add_task(task);
    self.storage().save(&day)?;

    Ok(task)
  }
}
