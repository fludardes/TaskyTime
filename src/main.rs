extern crate tasky_time;

use std::process;

use tasky_time::app;

fn main() {
  app::run(None, None).unwrap_or_else(|err| {
    eprintln!("Error occured: {}", err);
    process::exit(1)
  })
}
