use std::cell::{RefCell, Cell};
use std::rc::Rc;

use chrono::prelude::*;
use gio::prelude::*;
use gtk::prelude::*;

use chrono::naive::NaiveDate;
use gio;
use gtk;
use gtk::{
  Application, ApplicationWindow, Builder, Button, Calendar, Entry, IconTheme, ListBox, Popover, ListBoxRow, Label,
  Image
};
use std::time::Duration;

use super::AddTaskDialog;
use super::Timer;
use super::TimerStatus;
use super::TaskRow;
use conf;
use utils;
use App;
use Day;
use Task;

use Error;

type TaskList = Rc<RefCell<Vec<TaskRow>>>;

// GTK component clone is just pointer copy
#[derive(Clone)]
pub struct Ui {
  application: Application,
  window: ApplicationWindow,
  list_box_tasks: ListBox,
  button_change_day: Button,
  popover_change_day: Popover,
  calendar_change_day: Calendar,
  button_task_add: Button,
  button_task_edit: Button,

  button_task_track: Button,
  popover_task_track: Popover,
  button_task_track_confirm: Button,
  entry_task_track_time: Entry,

  button_task_delete: Button,
  dialog_add_task: AddTaskDialog,
  task_list: TaskList,
  timer: Timer
}

impl Ui {
  pub fn build() -> Result<Ui, Error> {
    debug!("UI for main window build run...");
    let application = gtk::Application::new(conf::APP_ID, gio::ApplicationFlags::empty())
      .map_err(|err| Error::with_reason("Application initialization failed", &err))?;

    if let Some(theme) = IconTheme::get_default() {
      let mut icons_path = utils::resources_path();
      icons_path.push("icons");
      theme.append_search_path(
        icons_path
          .to_str()
          .expect("Failed to parse icons path as UTF-8"),
      );
    } else {
      warn!("Failed to get icon theme, icons will not be used!");
    }

    let glade_src = include_str!("../../resources/ui/main_window.glade");
    let builder = Builder::new_from_string(&glade_src);

    debug!("UI for main window builded");

    let timer = Timer::build(&builder)?;

    let ui = Ui {
      application,
      window: builder_object(&builder, "main_window")?,
      list_box_tasks: builder_object(&builder, "list_box_tasks")?,
      button_change_day: builder_object(&builder, "button_change_day")?,
      popover_change_day: builder_object(&builder, "popover_change_day")?,
      calendar_change_day: builder_object(&builder, "calendar_change_day")?,
      button_task_add: builder_object(&builder, "button_task_add")?,
      dialog_add_task: AddTaskDialog::build()?,
      button_task_edit: builder_object(&builder, "button_task_edit")?,
      button_task_delete: builder_object(&builder, "button_task_delete")?,
      button_task_track: builder_object(&builder, "button_task_track")?,
      popover_task_track: builder_object(&builder, "popover_task_track")?,
      entry_task_track_time: builder_object(&builder, "entry_task_track_time")?,
      button_task_track_confirm: builder_object(&builder, "button_task_track_confirm")?,
      timer,
      task_list: Rc::new(RefCell::new(Vec::new())),
    };

    Ok(ui)
  }

  pub fn run(&self, app: Rc<App>) -> Result<(), Error> {
    let day = app.day();

    self.calendar_select_day(&day);
    self.show_day(&day);

    self.timer.run();

    {
      trace!("Connect calendar popover events...");
      let popover_change_day = self.popover_change_day.clone();
      self.button_change_day.connect_clicked(move |_| {
        popover_change_day.show_all();
        popover_change_day.popup();
      });
    }

    {
      debug!("Connect calendar events...");
      let ui = self.clone();
      let app = app.clone();
      self
        .calendar_change_day
        .connect_day_selected(move |_| ui.on_change_day(&app));
    }

    {
      trace!("Connect add task button...");
      let ui = self.clone();
      self
        .button_task_add
        .connect_clicked(move |_| ui.show_add_task_dialog());
    }

    {
      trace!("Connect edit task button...");
      let ui = self.clone();
      let app = app.clone();
      self
        .button_task_edit
        .connect_clicked(move |_| ui.show_edit_task_dialog(&app));
    }

    {
      trace!("Connect track time button...");
      let ui = self.clone();
      self
        .button_task_track
        .connect_clicked(move |_| ui.on_task_track_button_clicked());
    }

    {
      trace!("Connect delete task button...");
      let ui = self.clone();
      let app = app.clone();
      self
        .button_task_track_confirm
        .connect_clicked(move |_| ui.on_task_track_confirm(&app));
    }

    {
      trace!("Connect delete task button...");
      let ui = self.clone();
      let app = app.clone();
      self
        .button_task_delete
        .connect_clicked(move |_| ui.on_task_delete(&app));
    }

    {
      trace!("Connect list box row change...");
      let ui = self.clone();
      let app = app.clone();
      self
        .list_box_tasks
        .connect_row_selected(move |_, row| ui.on_row_selected(&app, row));
    }

    {
      trace!("Connect add task dialog confirm event..");
      let ui = self.clone();
      let app = app.clone();
      let dialog = self.dialog_add_task.clone();

      self.dialog_add_task.on_confirm(move |name, time| {
        if dialog.edit_mode() {
          ui.on_task_edit(&app, name, time);
        } else {
          ui.on_task_add(&app, name, time);
        }
      });
    }

    {
      let ui = self.clone();
      let app = app.clone();

      self.timer.connect_tick(move |time, status| {
        ui.on_timer_tick(&app, time, status);
      });
    }

    let window = self.window.clone();

    self.application.connect_startup(move |app| {
      window.set_application(app);
      window.connect_delete_event(move |win, _| {
        win.destroy();
        Inhibit(false)
      });
      window.show_all();
    });
    self.application.connect_activate(|_| {});
    self.application.run(&[]);

    Ok(())
  }

  fn on_timer_tick(&self, app: &App, time: Duration, status: TimerStatus) {
    trace!("Timer tick: {} (status: {:?})", utils::format_time_pretty(&time), &status);

    let number = if let Some(number) = self.selected_task_number() {
      number
    } else {
      self.timer.on_stop();
      return;
    };

    if status != TimerStatus::Run {
      return;
    }

    let task = match app.track_time(number.to_string(), Duration::from_secs(1)) {
      Ok(t) => t,
      Err(err) => {
        warn!("Failed to track time for task {}: {}", number, err);
        return;
      }
    };

    self.update_task_row(number - 1, &task);
  }

  pub fn on_row_selected(&self, _app: &App, row: &Option<ListBoxRow>) {
    if let Some(_row) = row {
      self.lock_widgets(false);
    } else {
      self.lock_widgets(true);
    }
  }

  pub fn lock_widgets(&self, lock: bool) {
    let sensitive = !lock;
    self.timer.set_sensitive(sensitive);
    //self.button_timer_start_pause.set_sensitive(sensitive);
    //self.label_timer.set_sensitive(sensitive);
  }

  pub fn calendar_select_day(&self, day: &Day) {
    let date = day.name();
    let date = if let Ok(date) = NaiveDate::parse_from_str(date.as_str(), "%Y-%m-%d") {
      date
    } else {
      error!("Failed to parse date \"{}\"", date.as_str());
      return;
    };

    self.calendar_change_day.set_property_day(date.day() as i32);
    let month = date.month() as i32 - 1;
    self.calendar_change_day.set_property_month(month);
    self.calendar_change_day.set_property_year(date.year());
  }

  pub fn on_task_add(&self, app: &App, name: String, time: Duration) {
    info!("Adding task...");
    let task = match app.add_task(name.as_str(), time) {
      Ok(task) => task,
      Err(err) => {
        error!(
          "Failed to add task with name {} and time {:?}: {}",
          name.as_str(),
          time,
          err
        );
        return;
      }
    };

    let task_row = TaskRow::new(&task);
    self.list_box_tasks.add(&task_row.widget());
    self.task_list.borrow_mut().push(task_row);
  }

  pub fn update_task_row(&self, row_number: usize, task: &Task) {
    let task_list = self.task_list.borrow();
    let task_list_item: Option<&TaskRow> = task_list.get(row_number);

    let task_row: TaskRow = if let Some(item) = task_list_item {
      item.clone()
    } else {
      warn!(
        "Failed to update row — can't get task widget in row {}!",
        row_number
      );
      return;
    };

    task_row.show_task(&task);
  }

  pub fn on_task_edit(&self, app: &App, name: String, time: Duration) {
    info!("Edit task...");

    let number = if let Some(number) = self.selected_task_number() {
      number
    } else {
      warn!("Failed to edit task — can't get selected task number!");
      return;
    };

    let task = match app.update_task(number.to_string(), Some(name.as_str()), Some(time)) {
      Ok(task) => task,
      Err(err) => {
        error!(
          "Failed to update task {} to name {} and time {:?}: {}",
          number,
          name.as_str(),
          time,
          err
        );
        return;
      }
    };

    self.update_task_row(number - 1, &task);
  }

  pub fn on_task_delete(&self, app: &App) {
    debug!("Delete task action...");

    let number = if let Some(number) = self.selected_task_number() {
      number
    } else {
      warn!("No selected task number!");
      return;
    };

    match app.remove_task(number.to_string()) {
      Ok(_) => (),
      Err(err) => {
        warn!("Failed to remove task {}: {}", number, err);
        return;
      }
    }

    let row = if let Some(row) = self.list_box_tasks.get_selected_row() {
      row
    } else {
      warn!("No task row selected");
      return;
    };

    self.list_box_tasks.remove(&row);
    self.task_list.borrow_mut().remove(number - 1);
  }

  pub fn on_task_track_button_clicked(&self) {
    self.popover_task_track.show_all();
    self.popover_task_track.popup();
  }

  pub fn on_task_track_confirm(&self, app: &App) {
    debug!("Track time action...");

    let time_str = if let Some(text) = self.entry_task_track_time.get_text() {
      text
    } else {
      warn!("No time string received from entry!");
      return;
    };

    let time = match utils::parse_time(&time_str) {
      Ok(t) => t,
      Err(err) => {
        warn!("Failed to parse time \"{}\": {}", time_str, err);
        return;
      }
    };

    let number = if let Some(number) = self.selected_task_number() {
      number
    } else {
      warn!("No selected task number!");
      return;
    };

    let task = match app.track_time(number.to_string(), time) {
      Ok(t) => t,
      Err(err) => {
        warn!("Failed to track time for task {}: {}", number, err);
        return;
      }
    };

    self.update_task_row(number - 1, &task);
    self.popover_task_track.popdown();
  }

  pub fn show_add_task_dialog(&self) {
    self.dialog_add_task.show();
  }

  pub fn selected_task_number(&self) -> Option<usize> {
    let row = if let Some(row) = self.list_box_tasks.get_selected_row() {
      row
    } else {
      warn!("No row selected!");
      return None;
    };

    let number = (row.get_index() + 1) as usize;
    debug!("Selected task number is {}", &number);
    Some(number)
  }

  pub fn selected_task(&self, app: &App) -> Option<Rc<Task>> {
    debug!("Trying to get selected task...");
    let number = if let Some(number) = self.selected_task_number() {
      number
    } else {
      return None;
    };

    let task = if let Some(task) = app.day().task_by_number(number) {
      task
    } else {
      warn!("Task with number {} not found!", number);
      return None;
    };

    Some(task)
  }

  pub fn show_edit_task_dialog(&self, app: &App) {
    debug!("Showing edit task dialog...");
    let task = if let Some(task) = self.selected_task(app) {
      task
    } else {
      warn!("Can't show edit task dialog for not found task");
      return;
    };

    trace!("Task found, show...");

    self.dialog_add_task.show_edit(&task);
  }

  pub fn show_day(&self, day: &Day) {
    let date = day.name();

    info!("Show day {}", date.as_str());

    self.button_change_day.set_label(date.as_str());

    trace!("Remove list box tasks children...");
    for (i, task_row) in self.list_box_tasks.get_children().iter().enumerate() {
      trace!("Remove list box item {}...", i);
      self.list_box_tasks.remove(task_row);
    }

    trace!("Clear list of task widgets...");
    self.task_list.borrow_mut().clear();

    debug!("Add {} day tasks...", day.tasks().len());
    for task in day.tasks().iter() {
      let task_row = TaskRow::new(&task);
      self.list_box_tasks.add(&task_row.widget());
      self.task_list.borrow_mut().push(task_row);
    }
  }

  pub fn on_change_day(&self, app: &App) {
    trace!("Changing day...");
    let day_num = self.calendar_change_day.get_property_day();
    let month_num = self.calendar_change_day.get_property_month() + 1;
    let year_num = self.calendar_change_day.get_property_year();

    trace!("Day {}, month {}, year {}...", day_num, month_num, year_num);

    let date: NaiveDate = NaiveDate::from_ymd(year_num, month_num as u32, day_num as u32);
    let date = date.format("%Y-%m-%d").to_string();

    debug!("Opening day {}...", &date);

    let day = match app.init_day(Some(date.as_str())) {
      Ok(d) => d,
      Err(err) => {
        error!("Failed to initialize day {}: {}", date, err);
        return;
      }
    };

    trace!("Day initialized");

    self.show_day(day);
  }
}

pub fn builder_object<S: AsRef<str>, W: ObjectExt>(builder: &Builder, id: S) -> Result<W, Error> {
  let id = id.as_ref();
  builder
    .get_object(id)
    .ok_or(Error::new(format!("UI object {} not found", id)))
}
