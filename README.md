# TaskyTime
**TaskyTime** — application for tracking time spent on tasks.

*Primary reason for me to develop this application is just studying Rust lang.  
Currently TaskyTime **not intended** for real usage!*

## Contents
* [Build](#build)
* [Usage](#usage)
* [Roadmap](#roadmap)

# Build
For development process cargo can be used.
```bash
cargo build
```

# Usage

## GUI
To use application in GUI mode, just run in (`cargo run`) without parameters — GTK user interface will be opened.  

## CLI
To use CLI, pass `--cli` flag or `task` subcommand. By default application uses current day to add, list, edit tasks. You can change this behavior and use previous (or future) day by using switch `-d` or `--day` to use specific date (in format `YYYY-MM-DD`).  
Example:
```bash
tasky-time -d "2018-08-27" task -l
```

### Add task
Add task with specific name
```bash
tasky-time task -a "My Task"
```

### List tasks
Option `-l` (`--list`) is used to list added tasks. In left-most column printed task numeric id (task order number in list). **Tasks are referenced** in update/remove/track operations by that **numeric id** (first column) or by exact **task name** (second column).
```bash
tasky-time task -l
```

### Edit task
Option `-u` (`--update`) allows to update task (referenced by **id** parameter, see above) info using switches `-n` (`--name`) to change task name, and/or `-t` (`--track`) to **reset** tracked time to specified value.
```bash
tasky-time task 2 -u --track "01:03:52" # Reset second task in list tracked time to 1h 3m 52s
tasky-time task "My Task" -u -n "New Name Task" # Change task name
```

### Remove task
Option `-r` (`--remove`) used to remove task (speficied by number or by name).
```bash
tasky-time task "My Task" --remove
```

### Track time
Option `-t` (`--track`) allows to track *additional* time for task. For example, you worked 40 minutes, go to break and just add these 40m to already tracked two hours.
```bash
tasky-time task 1 --track "40:00" # ⇒ 02:40:00
```

# Roadmap
Planned important features:

* [ ] - Graphical interface *(probably using GTK+ with gtk-rs)*
* [ ] - Store data in SQLite database
* [ ] - Simple CalDAV sync *(primarily for Nextcloud Tasks)*

## Features
Nice to have features:

- Live time tracking
- Serialized output *(for widgets and external interfaces)*
- Export data to CSV and other formats